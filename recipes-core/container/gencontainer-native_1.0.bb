SUMMARY = "Object-oriented filesystem paths"
DESCRIPTION = "pathlib offers a set of classes to handle filesystem paths. \
It offers the following advantages over using string objects: \
- No more cumbersome use of os and os.path functions. \
- Embodies the semantics of different path types. \
- Well-defined semantics, eliminating any warts or ambiguities. \
"
HOMEPAGE = "https://pypi.python.org/pypi/pathlib"
SECTION = "devel/python"
LICENSE = "CLOSED"


SRC_URI = "file://${BP}"

S="${WORKDIR}/${BP}"


inherit pythonnative
inherit setuptools
inherit native

DEPENDS += "python-pyyaml"
RDEPENDS_${PN} += " \
"
