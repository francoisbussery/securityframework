#!/usr/bin/env python2.7
import sys
from setuptools import setup


setup(
    name='genContainer',
    version='0.2',
    scripts=['src/genContainer.py',
             'src/ParseService.py'],
    platforms="platform-independent",
    package_dir={'':'src'},
    license='GPL v2',
    description='generate containers for image generaton',
    long_description=open('README').read(),
    author='Francois Bussery',
    author_email='francois.bussery@gmail.com',
    classifiers=[
        'Development Status :: 1 - Draft',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
        'Topic :: Software Development :: Libraries',
        'Topic :: System :: Filesystems',
        ],
)
