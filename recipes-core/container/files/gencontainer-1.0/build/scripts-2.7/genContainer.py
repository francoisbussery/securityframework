#!/usr/bin/python
import sys

__author__ = 'francoisbussery'

from optparse import OptionParser
from FilesystemFactory5 import FilesystemFactory5
import yaml
import os


class genContainer:
    def __init__(self, pathes, config):
        #
        #  need pathes=["WORKDIR":"/....","S":"/...."
        #       config=yaml_file
        self.pathes = pathes
        self.config = ""
        with open(config, 'r') as fd:
            self.config = yaml.load(fd)

    def area(self):
        return self.x * self.y


def main():
    parser = OptionParser()

    parser.add_option("-c", "--config", dest="yaml_config",
                      action="store", type="string",
                      help="GenContainer config file in Yaml")

    (options, args) = parser.parse_args()

    #
    # will retreive: yaml_config/path_manifests/path_images/path_tmp
    try:
        with open(options.yaml_config, 'r') as f:
            config = yaml.load(f)
            print "services:\n"
            for service_name in config["services"]:
                print "->%s\n" % service_name
            print "staging_input:%s" % config["staging_input"]
            print "image_output:%s" % config["image_output"]
        for option in config["options"]:
            print "->%s\n" % option

    except Exception as err:

       print parser.print_help()
       sys.exit(1)


if __name__ == "__main__":
    if os.name == "nt":
        DEVNULL = open("nul:", "w")
    else:
        DEVNULL = open("/dev/null", "w")
    main()
