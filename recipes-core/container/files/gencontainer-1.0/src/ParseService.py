#!/usr/bin/env python2.7
# coding=utf-8
"""
   Copyright 2016 François Bussery

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import os
import re
import yaml
import logging

from FilesystemFactory import FilesystemFactory
from utils import configuration
from utils import path

__author__ = 'francoisbussery'


# logging: debug/info/warning/error/critical
class ParseService:
    def __init__(self, path_staging, path_output, path_tmp, config_file):
        # type: (object, object, object, object) -> object
        #
        #  need pathes=["WORKDIR":"/....","S":"/...."
        #       config=yaml_file
        logging.info("Parse %s" % config_file)
        configuration().Set("path_host_rootfs",path(path_staging,True).onhost())
        configuration().Set("path_target_rootfs",path(path_output,True).onhost())
        configuration().Set("path_config_file", path(config_file,True).onhost())
        print "**--*** " + path(path_tmp,True).onhost()
        configuration().Set("path_tmp",path(path_tmp,True).onhost())
        configuration().Set("config_name",path(config_file,True).onhost())
        logging.debug(" path file=%s"%configuration().Get("path_config_file"))
        self.config = ""
        self.requiredfiles = []
        self.name=""
        try:
            fd = open(config_file, 'r')
        except IOError:
            assert isinstance(config_file, object)
            logging.error('cannot open %s' % config_file)
        else:
            self.config = yaml.load(fd)

    def WriteFilesystem(self):

        if not self.config["Filesystem"]:
            logging.error('missing Filesystem element')
            raise ValueError('missing Filesystem element')

        if not self.config["Filesystem"]["Files"]:
            raise ValueError('missing Files element')
        files = self.config["Filesystem"]["Files"]
        confname = path(configuration().Get("config_name"),True).filename()
        tmpdir = configuration().Get("path_tmp")
        try:
            os.stat(tmpdir)
        except:
            os.mkdir(tmpdir)
        logging.debug("load %s - %s " % (tmpdir, confname))
        workdir = os.path.join(tmpdir, confname)
        configuration().Set("workdir",workdir)
        try:
            os.stat(workdir)
        except:
            logging.debug( "workdir=%s" % workdir)
            os.mkdir(workdir)

        fdb = FilesystemFactory()

        for line in files:
            curfile = re.split(',| |\t|\n', line)
            curfile = filter(None, curfile)
            # if curfile.size()!=2:
            #     raise ValueError('problem with conf file: %s at %s'%(confname,file))
            file = curfile[0]
            perm = curfile[1]
            fdb.add(file,perm)

        self.requiredfiles=fdb.get()
        self.name=confname
        print fdb.get()
        fdb.write(self.GetUid(),self.GetGid())

    def GetFileList(self):
        return self.requiredfiles

    def GetName(self):
        return self.name

    def GetUid(self):
        return  self.config["Permissions"]["uid"]


    def GetGid(self):
        return self.config["Permissions"]["gid"]
