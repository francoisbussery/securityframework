#!/usr/bin/env python2.7
# coding=utf-8
"""
   Copyright 2016 François Bussery

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import sys


__author__ = 'francoisbussery'

from optparse import OptionParser
from ParseService import ParseService
from utils import path
import yaml
import os
import logging



class genContainer:
    def __init__(self, pathes, config):
        #
        #  need pathes=["WORKDIR":"/....","S":"/...."
        #       config=yaml_file
        self.pathes = pathes
        self.config = ""
        with open(config, 'r') as fd:
            self.config = yaml.load(fd)


def main():
    parser = OptionParser()

    parser.add_option("-c", "--config", dest="yaml_config",
                      action="store", type="string",
                      help="GenContainer config file in Yaml")

    parser.add_option("-l", "--log", dest="debug",
                      action="store", type="string",
                      help="logging verbosity (debug/info/warning/error/critical)")


    (options, args) = parser.parse_args()
    dbglevel={"debug":logging.DEBUG,"info":logging.INFO,"warning":logging.WARNING,"error":logging.ERROR,"critical":logging.CRITICAL}
    #
    # will retreive: yaml_config/path_manifests/path_images/path_tmp
    if 1:
        logging.basicConfig(format='%(asctime)s [%(filename)s:%(funcName)s():%(lineno)s ] - %(levelname)s:%(message)s',
                            level=dbglevel[options.debug])

        with open(options.yaml_config, 'r') as f:
            config = yaml.load(f)
            logging.debug( "staging_input:%s" % path(config["staging_input"],True).onhost())
            logging.debug("image_output:%s" % path(config["image_output"],True).onhost())
            logging.debug("temp dir%s" % path(config["temp_dir"],True).onhost())
            for service_name in config["services"]:
                logging.debug( "service : %s" % service_name)
                service=ParseService(path(config["staging_input"],True).onhost(), \
                             path(config["image_output"],True).onhost(), \
                             path(config["temp_dir"],True).onhost(),path(service_name,True).onhost())

                service.WriteFilesystem()

    #except Exception as err:

     #  print parser.print_help()
      # sys.exit(1)


if __name__ == "__main__":
    if os.name == "nt":
        DEVNULL = open("nul:", "w")
    else:
        DEVNULL = open("/dev/null", "w")
    main()
