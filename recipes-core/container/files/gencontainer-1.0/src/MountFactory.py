#!/usr/bin/env python2.7
# coding=utf-8
"""
   Copyright 2016 François Bussery

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

__author__ = 'francoisbussery'
import logging
import os

from utils import configuration
from utils import path

PermConvert = {"r": "400", "rx": "500", "rw": "600", "link": "link"}


class MountFactory:
    def __init__(self):
        self.sb = []

    def add(self, mntname):
        # ex: - /run /run (bind) noatime
        mount_type={"(bind)":"bind","(tmpfs)":"tmpfs","(sysfs)":"sysfs","(cgroup)":"cgroup"}
        logging.debug('add mnt:  %s' % mntname)
        split_mntname=mntname.split(" ")
        split_mntname= filter(None, split_mntname)
        try:
            scr=split_mntname[0]
            dst=split_mntname[1]
            type=mount_type[split_mntname[2]]
            option=split_mntname[3]
        ex




        cur_file = path(file)
        cur_perm = perm
        if cur_file.filetype() == "slink":
            cur_perm = "link"

        logging.debug('add %s' % file)
        if cur_file.filename() == "**":
            logging.debug('search path:  %s' % cur_file.onhost_dir())
            for root, dirs, files in os.walk(cur_file.onhost_dir()):
                filelst = FilesystemFactory()
                relative_path = root.replace(configuration().Get("path_host_rootfs"), "")
                for f in files:
                    logging.debug('match is:  %s' % os.path.join(relative_path, f))
                    filelst.add(os.path.join(relative_path, f), perm)
                    self.sb = self.sb + filelst.get()
        else:
            cur_file = path(file)
            if not cur_file.exist():
                pass
            else:
                self.sb.append((cur_file.onhost(), cur_perm))
                if cur_file.filetype() == "slink":
                    slink = path(os.readlink(cur_file.onhost()))
                    if slink.ontarget()[0] == ".":
                        slink = path(cur_file.ontarget_dir() + slink.ontarget())
                        logging.debug('linked to %s' % slink.ontarget())
                    filelst = FilesystemFactory()
                    filelst.add(slink.ontarget(), perm)
                    self.sb = self.sb + filelst.get()
                elif cur_file.filetype() in ["exec", "library", "module"]:
                    cmd = "mklibs-readelf --print-needed %s " % cur_file.onhost()
                    stdout = os.popen(cmd)
                    for lib in stdout:
                        lib = lib.replace("\n", "")
                        self.sb.append((path(lib).onhost(), cur_perm))

    def get(self):
        return self.sb

    def write(self, uid, gid):
        workpath = configuration().Get("workdir")
        logging.info("create intermediate rootfs in  %s" % workpath)
        for file in self.get():
            name = file[0]
            try:
                perm = PermConvert[file[1]]
            except:
                logging.error("permission not valid file: %s has perm %s" % (name, file[1]))
                raise ("error permission")
            src = path(name)
            relatpath = src.ontarget_dir()
            targetpath = workpath + relatpath
            cmd = "mkdir -p " + targetpath
            stdout = os.popen(cmd)
            logging.debug(cmd)
            cmd = "cp -f " + src.onhost() + " " + targetpath
            stdout = os.popen(cmd)
            logging.debug(cmd)
            if perm != "link":
                cmd = "chmod " + perm + " " + targetpath + src.filename()
                logging.debug(cmd)

                stdout = os.popen(cmd)
                cmd = "chown  %s:%s %s" % (uid, gid, targetpath + src.filename())
                logging.debug(cmd)
