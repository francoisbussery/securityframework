#!/usr/bin/env python2.7
# coding=utf-8
"""
   Copyright 2016 François Bussery

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""
__author__ = 'francoisbussery'

import sys
import os
import logging
from utils import configuration
from utils import path
from utils import file_db


PermConvert={"r":"400","rx":"500","rw":"600","link":"link"}
class GenerateIntermediateRootfs(file_db):
    def __init__(self, service):
        logging.info("create intermediate rootfs for  %s" % service.GetName())
        workpath=configuration().Get("workdir")
        for file in service.GetFileList():
            name=file[0]
            try:
               perm=PermConvert[file[1]]
            except:
                logging.error("permission not valid file: %s has perm %s" % (name,file[1]))
                raise("error permission")
            src=path(name)
            relatpath=src.ontarget_dir()
            targetpath=workpath + relatpath
            cmd="mkdir -p "+targetpath
            stdout=os.popen(cmd)
            logging.debug(cmd)
            cmd = "cp -f "+src.onhost()+ " "+targetpath
            stdout=os.popen(cmd)
            logging.debug( cmd)
            if perm!="link":
              cmd  = "chmod " + perm + " " + targetpath + src.filename()
              logging.debug(cmd)


              stdout=os.popen(cmd)
              cmd = "chown  %s:%s %s"%( service.GetUid(),service.GetGid(),targetpath + src.filename() )
              logging.debug(cmd)


