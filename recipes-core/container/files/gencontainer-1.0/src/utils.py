#!/usr/bin/env python2.7
# coding=utf-8
"""
   Copyright 2016 François Bussery

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""


__author__ = 'francoisbussery'
import os
import logging


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class configuration(object):
    __metaclass__ = Singleton

    def __init__(self):
        self.pathdict = {}

    def Set(self, name, path):
        self.pathdict[name] = path

    def Get(self, name):
        return self.pathdict[name]


class path:
    def __init__(self, pathname,absolute=False):
        #        print "path(%s,%s)"%(pathname,base_path)

        self.relapath = self.normalize(self.expend_pathname(pathname))
        if not absolute:
            self.relapath=self.relapath.replace(configuration().Get("path_host_rootfs"), "")
        self.absolute=absolute
        name = os.path.basename(pathname)
        # search in /lib
        #   print "found name=----%s----"%name
        #   print "found base=%s"%self.basepath
        #   print "abspath=%s"%self.abspath
        # we just have the name... n path. we have to search for it
        if (name == self.relapath):
            logging.debug("no pathname. we need to search it")
            hostpath = configuration().Get("path_host_rootfs")
            for root, dirs, files in os.walk("%s/lib" % hostpath):
                if name in files:
                    self.relapath = os.path.join(root, name).replace(hostpath, "")
                    logging.debug("match %s !!!!!! root=%s host=%s\n"%(self.relapath,root,hostpath))
                    return
                    # then test first in /usr/lib
            for root, dirs, files in os.walk("%s/usr/lib" % hostpath):
                if name in files:
                    self.relapath = os.path.join(root, name).replace(hostpath, "")
                    logging.debug("match %s !!!!!! root=%s host=%s\n"%(self.relapath,root,hostpath))

                    return
                    # therest
            for root, dirs, files in os.walk("%s" % hostpath):
                if name in files:
                    self.relapath = os.path.join(root, name).replace(hostpath, "")
                    logging.debug("match %s !!!!!! root=%s host=%s\n"%(self.relapath,root,hostpath))
                    return
                    # raise ValueError('file %s not found' % name)

    def normalize(self, path):
        if (path[0]=='/'):
            prefix = "/"
        else:
            prefix=""
        splittedpath = path.split("/")
        splittedpath = filter(None, splittedpath)
        return prefix+'/'.join(splittedpath)

    def expend_pathname(self, path):
        # type: (object) -> object

        splittedpath = path.split("/")
        if (path[0]=='/'):
            fullpath = "/"
        else:
            fullpath=""
        for elem in splittedpath:
            if not elem:
                continue
            if elem[0] == "$":
                cur_elem = elem
                elem = os.getenv(elem[1:], None)
                #       print "getenv: %s"%elem
                if elem == None:
                    logging.warning('missing environement variable %s' % cur_elem)
                    raise ValueError('missing environement variable %s' % cur_elem)

            if fullpath != "":
                fullpath = fullpath + "/" + elem
            else:
                fullpath = elem
        # print "fullpath=%s"%fullpath

        return fullpath

    def onhost_dir(self):
        return configuration().Get("path_host_rootfs") + self.relapath.replace(self.filename(), "")

    def ontarget_dir(self):
        return self.relapath.replace(self.filename(), "")

    def onhost(self):
        if self.absolute:
            return self.relapath
        else:
          return configuration().Get("path_host_rootfs") + self.relapath

    def ontarget(self):
        return  configuration().Get("path_target_rootfs") + self.relapath

    def filename(self):
        return os.path.basename(self.relapath)

    def exist(self):
        return os.path.isfile(self.onhost())

    def filetype(self):
        # if os.stat()
        if not self.exist():
            return "none"
        if os.path.islink(self.onhost()):
            return "slink"
        stdout = os.popen("readelf -h %s | grep Type" % self.onhost()).readline()

        if stdout.find('REL') >= 0:
            return "module"
        if stdout.find('DYN') >= 0:
            return "library"
        if stdout.find('EXEC') >= 0:
            return "exec"
        with open(self.onhost()) as f:
            firstline = f.readline()
            if firstline.find("#!/") >= 0:
                return "script"
        if self.onhost()[-3:] == ".sh":
            return "script"
        return "file"

