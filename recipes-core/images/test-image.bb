SUMMARY = "A small image just capable of allowing a device to boot."
SRC_URI = "file://${BPN}"
LICENSE = "CLOSED"
S="${WORKDIR}/${BPN}"

IMAGE_INSTALL = "packagegroup-core-boot ${ROOTFS_PKGMANAGE_BOOTSTRAP} ${CORE_IMAGE_EXTRA_INSTALL}"
GEN_CONTAINER_CONF="image.conf"

LICENSE = "MIT"

IMAGE_PREPROCESS_COMMAND="do_fetch;do_unpack;process_container"
inherit container-image
inherit core-image
