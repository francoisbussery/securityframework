# Security Framework #

The security framework is an openembeded layer.
Its aim is to generate a full secured image based on yocto.

The image is now a collection of services you want to put in your openembedded project.
(exemple here: recipes-core/images)

 So, the image is fully described by

* A configuration file that list all services in yaml format
* All services in Yaml format


### What does the security framework ###

* Generate optimized jail for each services
* Generate optimize permissions
* Generate the apparmor configuration
* Generate lxc start scripts
