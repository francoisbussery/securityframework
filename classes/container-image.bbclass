

IMAGE_FEATURES[validitems] +="disable_network_ns"
IMAGE_FEATURES[validitems] +="disable_container"
IMAGE_FEATURES[validitems] +="disable_filesystem_optim"
IMAGE_FEATURES[validitems] +="disable_absolute_link"
IMAGE_FEATURES[validitems] +="disable_apparmor"
IMAGE_FEATURES[validitems] +="use_lxc"
IMAGE_FEATURES[validitems] +="use_pflask"

# load all .yaml files and process them
# load all .tgz files for rootfs minimal and process it

export PYTHONPATH="${STAGING_LIBDIR_NATIVE}/python2.7/site-packages"
DEPENDS = "gencontainer-native mklibs-native"

#     if (bb.utils.contains('EXTRA_IMAGE_FEATURES', 'disable-fs-optim', False, True, d)):
#       print "jj"

export IMAGE_PWD="${IMAGE_ROOTFS}"
export PWD="${WORKDIR}"
export SOURCE="${S}"

fakeroot process_container() {

  genContainer.py --config ${GEN_CONTAINER_CONF}

}
